#include <stdio.h>
#include <stdlib.h>

int main()
{
    char c;
    int lowerCase, upperCase;
    printf("Enter a letter:");
    scanf("%c", &c);

    lowerCase=(c =='a' || c =='e' || c =='i' || c =='o' || c=='u');
    upperCase=(c =='A' || c =='E' || c =='I' || c =='O' || c =='U');

    if (lowerCase||upperCase){
        printf("It is a vowel");
    }
    else{
        printf("It is a consonant");
    }
    return 0;
}
